# onion-web-ui

已经完成的部分

1. 基本组件框架
2. 文档系统的搭建
3. 单元测试框架的引入
4. 引入组件的模版语法提示
5. 分包模式的搭建

未完成的部分

1. ~~valor 插件的模版语法提示 （P0）~~
2. 生成组件的 cli 工具 与 playgound 的搭建
3. ~~拆分为多包模式 （为 cli 和 playground 做准备）~~
4. 文档系统自动生成目录

待优化部分

1. rollup 打包的错误提示消除
2. hasky 提交前的检查
3. stylelint 的引入
4. 文档样式优化

其他

1. [vitepress 使用教程](https://vitepress.yiov.top/)
2. [文档预览地址](https://based-naive-components.vercel.app/components/button.html)

问题

1. 使用 vite-plugin-dts 插件报错 type 关键字问题
