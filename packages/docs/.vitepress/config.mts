import { defineConfig } from "vitepress";
import { componentPreview, containerPreview } from '@vitepress-demo-preview/plugin'
// @ts-ignore
import AutoNavPlugin from 'vitepress-auto-nav-sidebar'
import VueJsx from '@vitejs/plugin-vue-jsx'

const { nav, sidebar } = AutoNavPlugin({
  entry: './',
  ignoreFolders: ["cache", "demos", ".vitepress", "node_modules", "doc",], // 需要排除的一些目录
  ignoreFiles: ['api-examples', 'index', 'markdown-examples'], // 需要排除的一些文件
  dirPrefix: '',
  filePrefix: '',
  showNavIcon:true,
  showSideIcon:true,
  collapsed: false,
  singleLayerNav: false,
  customParentFolderName: '',
})

// https://vitepress.dev/reference/site-config
export default defineConfig({
  title: "组件库",
  description: "一个基于 naive ui 组件库",
  markdown: {
    config(md) {
      md.use(componentPreview)
      md.use(containerPreview)
    }
  },
  // base: `/v${process.env.npm_package_version}/`,
  // outDir: `./.vitepress/dist/v${process.env.npm_package_version}`,
  themeConfig: {
    // https://vitepress.dev/reference/default-theme-config
    // nav: [
    //   { text: "Home", link: "/" },
    //   { text: "Examples", link: "/markdown-examples" },
    // ],
    // nav,
    // @ts-ignore
    sidebar: JSON.parse(JSON.stringify(sidebar).replaceAll('//', '/')) ,
    socialLinks: [
      { icon: "github", link: "https://github.com/vuejs/vitepress" },
    ],
  },
  vite: {
    // Vite 配置选项
    ssr: {
      noExternal: ["naive-ui"],
    },
    plugins: [
      // @ts-ignore
      VueJsx({}),
    ]
  },
});
