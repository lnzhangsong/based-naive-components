---
# https://vitepress.dev/reference/default-theme-home-page
layout: home

hero:
  name: "一个组件库"
  text: "组件库"
  tagline: "念念不忘 必有回想"
  actions:
    - theme: brand
      text: 组件
      link: /components/按钮.html

features:
  - title: Feature A
    details: Lorem ipsum dolor sit amet, consectetur adipiscing elit
---
