import { App } from 'vue'
import * as components from './components/index'
export * from './components/index'

const installer = (app: App) => {
	// @ts-ignore
	Object.keys(components).forEach(componentKey => app.use(components[componentKey]))
}

export default {
	install: installer,
	...components
}


