import { ExtractPropTypes } from 'vue';

export type ButtonType = 'success' | 'danger' | 'warning' | 'info' | 'simple';

export const buttonProps = {
  type: {
    type: String as () => ButtonType,
    default: 'info',
  },
} as const;

export type ButtonProps = ExtractPropTypes<typeof buttonProps>;
