import { withInstall } from "../../utils/install";

import Card from "./src";

export const ACard = withInstall(Card);
export default ACard;
export type { ButtonType } from './types'
