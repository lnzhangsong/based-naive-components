import { NButton } from "naive-ui";
import { defineComponent } from "vue";
import "./index.scss";
import { buttonProps } from "../types";

export default defineComponent({
  name: "ACard",
  props: buttonProps,
  setup() {},
  render() {
    return <NButton type="primary" size="large"> button </NButton>;
  },
});
